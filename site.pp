package { 'httpd':
 ensure => present, 
}

file { '/var/www/html':
 ensure => directory, 
}

file { '/var/www/html/index.html':
 ensure => 'file', source => '/etc/puppetlabs/puppet/files/index.html', 
}

service { 'httpd':
 ensure => running, 
}
